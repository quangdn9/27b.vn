var APP = APP || {};

APP.authProfile = function(FB, $, text, loggedIn, track){
    var _isLoggedIn = loggedIn;
    var _track      = track;
    var _text       = text;
    var _url        = {
        api: '/api/auth',
        form: '/brand_permission_profile'
    };
    var _log = function(o) {
        // console.log(o);
    };

    var _profile = {
        save: function(){
            $.ajax({
                url: _url.api,
                type: "POST",
                data: JSON.stringify({
                    "userID" : $('#userID').val(),
                    "accessToken" : $('#accessToken').val(),
                    "name" : $('#first_name').val() + " " + $('#last_name').val(),
                    "fb_name" : $('#fb_name').val(),
                    "first_name" : $('#first_name').val(),
                    "last_name" : $('#last_name').val(),
                    "company_name" : $('#company_name').val(),
                    "company_title" : $('#company_title').val(),
                    "company_email" : $('#company_email').val(),
                    "company_phone" : $('#company_phone').val(),
                    "company_client" : $('#company_client').val(),
                    "fb_contact_email" : $('#fb_contact_email').val(),
                    "CSRFName": $('input[name="CSRFName"]').val(),
                    "CSRFToken": $('input[name="CSRFToken"]').val()
                }),
                contentType: "application/json",
                dataType: "json"
            }).done(function(data, textStatus, jqXHR) {
                if (data.status === 3) {
                    // TODO: Show an error to the user.
                    //_displayErrors(data.reason);
                    _log('Error on save');
                } else {
                    $(document).trigger('close.facebox');
                    window.location.reload(true);
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                // TODO: Show an error to the user.
                _log(textStatus);
                _log(jqXHR);
                _log(errorThrown);
            });
        },
        form: function(data) {
            $.get(_url.form, data, function(response) {
                if (response.status === false){
                    jQuery.facebox(response.content);
                    $('#save').click( function() {
                        // TODO: Show loading animation.
                        _profile.save();
                    });
                }
                else {
                    window.location.reload(true);
                }
            }, "json");
        }
    };
    
    var _showLoggedOut = function () {
        $('.login-label').text(_text.notLoggedIn);
        $('.ident-name').text(_text.logIn); 

        $('.login-identity')
            .removeClass('menu-btn')
            .addClass('login-btn');
        $('.ident-menu').hide();
    };

    var _login = function(response, cb) {
        if (response.status === 'connected') {
            $.ajax({
                url: _url.api,
                type: "PUT",
                data: JSON.stringify({
                    id : response.authResponse.userID,
                    name: response.authResponse.name,
                    first_name: response.authResponse.first_name,
                    last_name: response.authResponse.last_name,
                    locale: response.authResponse.locale,
                    timezone: response.authResponse.timezone
                }),
                contentType: "application/json",
                dataType: "json",
                success: function(data) {
                    if (data.status === 0) {
                        _profile.form(response.authResponse);
                    } else {
                        window.location.reload(true);
                    }
                   // if (_track) mixpanel.track("Login");
                    if (typeof cb == 'function') cb();
                }
           });
        }
    };

    var _toggleLoginMenuDropdown = function() {
        if ($('.ident-menu').css('display') == 'block'){
            $('.ident-menu').hide();
        } else {
            $('.ident-menu').show();
        }
    };
    
    var _logout = function(cb){
        $('.login-identity').unbind('click');
        _showLoggedOut();
        $.ajax({
            url: _url.api,
            type: "DELETE",
            contentType: "application/json",
            dataType: "json"
        }).done(function(){
            // FB.logout();
            if (typeof cb == 'function') cb();
            window.location.reload(true);
        });
    };
    
    var _cancel = function (response){
        _showLoggedOut();
    };

    var _loginClicked = function () {
            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    // the user is logged in and has authenticated your
                    // app, and response.authResponse supplies
                    // the user's ID, a valid access token, a signed
                    // request, and the time the access token 
                    // and signed request each expire
                    // var uid = response.authResponse.userID;
                    // var accessToken = response.authResponse.accessToken;
                    if (response.authResponse) {
                        _login(response);
                    } else {
                        _cancel(response);
                    }
                } else if (response.status === 'not_authorized') {
                    // the user is logged in to Facebook, 
                    // but has not authenticated your app
                    FB.login(function(response) {
                        if (response.authResponse) {
                            _login(response);
                        } else {
                            _cancel(response);
                        }
                    });
                } else {
                    // the user isn't logged in to Facebook.
                    FB.login(function(response) {
                        if (response.authResponse) {
                            _login(response);
                        } else {
                            _cancel(response);
                        }
                    });
                }
            });
    };

    //
    // Install click handlers, etc.
    //
    
    if (_isLoggedIn) {
        $('.login-identity').click(function(e){
            _toggleLoginMenuDropdown();
            e.stopPropagation();
        });
    
        $('.logout').click(function(){
            _logout();
        });
    } else {
        $('.login-identity').click(function(e) {
            _loginClicked();
        });
    }

    if (_track) {
        if (_isLoggedIn) {
          //  mixpanel.register({ 'Login Status': 'Known' });
        } else {
          //  mixpanel.register({ 'Login Status': 'Anonymous' });
        }
    }

    return {
        login: _login,
        logout: _logout
       // track: _track
    };
};
